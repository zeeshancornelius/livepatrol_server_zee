/**
 * Created by DOStech
 */
var crypto = require('crypto');
var express = require('express');
var app = express();
const server = require('http').Server(app)
	, io = require('socket.io')(server)
	, path = require('path')
	, rtsp = require('rtsp-ffmpeg');
var cookieParse = require('cookie-parser');
var onvifCam = require('onvif').Cam;
var bodyParser = require('body-parser');
var session = require('client-sessions');
var mysql      = require('mysql');
//Set pool setting with DB
var pool  = mysql.createPool({
	host     : 'livepatroldb.c2edw7psi0ut.us-west-2.rds.amazonaws.com',
	user     : 'zeeshan',
	password : 'zeeshan123',
	database: 'livepatrol_db',
	port     : '3306'

});

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});
app.use(cookieParse());
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use('/public', express.static(__dirname + '/public'));
app.use(session({
	cookieName: 'session',
	secret: '123',
	duration: 30 * 60 * 1000,
	activeDuration: 5 * 60 * 1000,
}));

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

//Check Connection is established with DB
pool.getConnection(function(err, connection) {
	if (err) {
		throw err;
	} else {
		console.log('DB Connection Establish');
	}
});

function checkUserCredential(username, password, callback){
	pool.getConnection(function(err, connection) {
		connection.query("select * from user where username = '"+username+"' && password = '"+password+"'", function(err, result) {
			if (err){
				callback("Error Communicating " + err);
			} else if (result.length == 0) {
				callback(0);
			} else {
				callback(result);
			}
		});
		connection.release();
	});
}

function getUserSites(userId, callBack) {
	pool.getConnection(function(err, connection) {
		connection.query("select * from user_sites where user_id = '"+userId+"'", function(err, result) {
			if (err){
				callBack("Error Communicating " + err);
			} else if (result.length == 0) {
				callBack(0);
			} else {
				callBack(result);
			}
		});
		connection.release();
	});
}

function getUserSiteCameras(userId, siteId, callBack) {
	pool.getConnection(function(err, connection) {
		connection.query("select * from cameras where site_id = '"+siteId+"'", function(err, result) {
			if (err){
				callBack("Error Communicating " + err);
			} else if (result.length == 0) {
				callBack(0);
			} else {
				callBack(result);
			}
		});
		connection.release();
	});
}

app.post('/login', function(req, res){
	var userName = req.body.user.username;
	var password = crypto.createHash('md5').update(req.body.user.password).digest('hex');
	if (req.body.user.device == "web"){
		if (userName == "" ||  password == ""){
			res.render("login", {
				"message" : "Username and Password cannot be empty!"
			});
		} else {
			console.log("Password : " + password);
			var html = 'Hello: ' + userName + '.<br>' +
				'<a href="/">Try again.</a>';
			checkUserCredential(userName, password, function (err) {
				if (err == 0) {
					res.render("login", {
						"message" : "Invalid Credentials!"
					});
				} else {
					var tmpUsrId = err[0].id;
					getUserSites(tmpUsrId, function (result) {
						if (result == 0) {
							req.session.userId = err[0].user_id;
							req.session.user = userName;
							req.session.isLogin = true;

							if(res.cookie('user_id', tmpUsrId)){
								res.sendFile(__dirname + '/index.html');
							}
						} else {
							req.session.userId = err[0].id;
							req.session.user = userName;
							req.session.isLogin = true;
							res.cookie('site_id', result[0].site_id);

							if(res.cookie('user_id', tmpUsrId)){
								res.sendFile(__dirname + '/index.html');
							}
						}
					});
				}
			});
		}
	} else if(req.body.user.device == "mobile") {
		if(userName == "" ||  password == "") {
			var feedback = {
				status: 0,
				err_msg: "Username or Password is empty!!"
			};
			res.json(feedback);
			return;
		} else {
			checkUserCredential(userName, password, function (err) {
				if (err == 0) {
					var feedback = {
						status: 0,
						err_msg: "Invalid Credentials!"
					};
					res.json(feedback);
					return;
				} else {
					var tmpUsrId = err[0].id;
					getUserSites(tmpUsrId, function (result) {
						if (result == 0) {
							if(res.cookie('user_id', tmpUsrId)){
								var data = {
									status: 1,
									userId : err[0].id,
									username : userName,
									firstname : err[0].firstname,
									lastname : err[0].lastname
								};
								res.json(data);
								return;
							}
						} else {
							if(res.cookie('user_id', tmpUsrId)){
								res.cookie('site_id', result[0].id);
								req.session.isLogin = true;
								var data = {
									status: 1,
									userId : err[0].id,
									username : userName,
									firstname : err[0].firstname,
									lastname : err[0].lastname
								};
								res.json(data);
								return;
							}
						}
					});
				}
			});
		}
	}
});





app.get('/userSites', function (req, res){
	var cookie_string = req.headers.cookie;
	var ua = req.headers['user-agent'];
	var tmpJson = parseCookiesString(cookie_string);
	getUserSites(tmpJson.user_id, function (err) {
		console.log(err);
		if (err == 0) {
			console.log("No Sites");
		}else{
			res.send(JSON.stringify(err));
		}
	});
	
});

app.post('/userSiteCameras', function (req, res) {
	console.log("I am here! And " + req.body.user.device);
	if (req.body.user.device == "web"){
		if (typeof req.session.isLogin !== 'undefined' && req.session.isLogin) {
			if (typeof req.body.user.site_id !== 'undefined' && req.body.user.site_id != "") {
				var userSiteId = req.body.user.site_id;
				res.cookie('site_id', userSiteId);
				res.sendFile(__dirname + '/index.html');
			} else {
				res.render('login',{
					'message' : ''
				});
			}
		} else {
			res.render('login',{
				'message' : ''
			});
		}
	} else if(req.body.user.device == "mobile") {
		if (typeof req.session.isLogin !== 'undefined' && req.session.isLogin) {
			if (typeof req.body.user.site_id !== 'undefined' && req.body.user.site_id != "") {
				var userSiteId = req.body.user.site_id;
				res.cookie('site_id', userSiteId);
				var data = {
					status: 1,
					success_msg: "Successfully site selected.",
					site_id: userSiteId,
				};
				res.json(data);
				return;
			} else {
				var feedback = {
					status: 0,
					err_msg: "Site does not exit!"
				};
				res.json(feedback);
				return;
			}
		} else {
			var feedback = {
				status: 0,
				err_msg: "Site does not exit!"
			};
			res.json(feedback);
			return;
		}
	}
});

app.get('/logout', function (req, res) {
	if(req.session && req.session.user){
		req.session.reset();
		res.clearCookie('user_id');
		res.clearCookie('site_id');
		res.redirect('/');
	}else{
		res.render('login',{
			'message' : ''
		});
	}
});

// use rtsp = require('rtsp-ffmpeg') instead if you have install the package
server.listen(8080, function(){
	console.log('Listening on localhost:8080');
});


io.on('connection', function(socket) {
	var cookie_string = socket.request.headers.cookie;
	var ua = socket.request.headers['user-agent'];
	var tmpJson = parseCookiesString(cookie_string);
	console.log("This is User Id : " + tmpJson.user_id);
	console.log("This is Site Id : " + tmpJson.site_id);
	var camsUsers = [];
	getUserSiteCameras(tmpJson.user_id, tmpJson.site_id, function (err) {
		if (err == 0) {
			socket.emit('start', err.length, tmpJson.user_id)
			console.log("Sorry there are no cameras configured for you yet");
		}else{
			for(i = 0; i<err.length ; i++){
				camsUsers.push(err[i].rtsp_url);
			}
			console.log(camsUsers);
			var cams=camsUsers.map(function(uri, i) {
				var stream = new rtsp.FFMpeg({input: uri, resolution: '2048x1536', quality: 13, isMobileDevice: checkRequestFromMobile(ua)});
				stream.on('start', function() {
					console.log('stream ' + i + ' started');
				});
				stream.on('stop', function() {
					console.log('streaming ' + i + ' stopped');
				});
				return stream;
			});

			cams.forEach(function(camStream, i) {
				var ns = io.of('/cam' + tmpJson.user_id + i);
				ns.on('connection', function(wsocket) {
					console.log('connected to /cam' + tmpJson.user_id + i);
					var pipeStream = function(data, index) {
						wsocket.emit('data', data, i);
					};
					camStream.on('data', pipeStream, i);
					wsocket.on('disconnect', function() {
						console.log('disconnected from /cam' + i);
						camStream.removeListener('data', pipeStream, i);
					});
				});
			});
			socket.emit('start', cams.length, tmpJson.user_id);
		}
	});
});

function parseCookiesString (rc) {
	var list = {};

	rc && rc.split(';').forEach(function( cookie ) {
		var parts = cookie.split('=');
		list[parts.shift().trim()] = decodeURI(parts.join('='));
	});

	return list;
}

function checkRequestFromMobile(ua) {
	var isMobileDevice = false;
	if (/Android|webOS|iPhone|iPad|BlackBerry|Windows Phone|Opera Mini|IEMobile|Mobile/i.test(ua)) {
		isMobileDevice = true;
	}
	return isMobileDevice;
}

app.get('/', function (req, res) {
	if(req.session && req.session.user){
		res.sendFile(__dirname + '/index.html');
	}else{
		res.render('login',{
			'message' : ''
		});
	}
	// } else {
	// 	res.sendFile(__dirname + '/index.html');
	// }
});

app.post('/checkCameraUrl', function (request, response) {

	var CAMERA_HOST = request.body.cameraHost,
		USERNAME = request.body.cameraUsername,
		PASSWORD = request.body.cameraPassword,
		PORT = request.body.cameraPort;

	if (typeof CAMERA_HOST !== 'undefined' && CAMERA_HOST) {
		if (typeof USERNAME !== 'undefined' && USERNAME) {
			if (typeof PASSWORD !== 'undefined' && PASSWORD) {
				if (typeof PORT !== 'undefined' && PORT) {
					new onvifCam({
						hostname: CAMERA_HOST,
						username: USERNAME,
						password: PASSWORD,
						port: PORT
					}, function (err) {
						if (err) {
							console.log(err);
							var failResp = {
								status: 0,
								error_msg: 'Connection Failed for ' + CAMERA_HOST + ' Port: ' + PORT + ' Username: ' + USERNAME + ' Password: ' + PASSWORD
							};
							response.json(failResp);
							return;
						}

						this.getStreamUri({protocol: 'RTSP'}, function (err, stream) {
							var succResp = {
								status: 1,
								data: stream
							};
							response.json(succResp);
							console.log(stream);
						});
					});
				} else {
					var failResp = {
						status: 0,
						error_msg: "Port empty!"
					};
					response.json(failResp);
					return;
				}
			} else {
				var failResp = {
					status: 0,
					error_msg: "Password empty!"
				};
				response.json(failResp);
				return;
			}
		} else {
			var failResp = {
				status: 0,
				error_msg: "Username empty!"
			};
			response.json(failResp);
			return;
		}
	} else {
		var failResp = {
			status: 0,
			error_msg: "Camera Host empty!"
		};
		response.json(failResp);
		return;
	}
})
